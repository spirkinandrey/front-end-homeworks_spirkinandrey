import React from 'react';
import Modal from "./Modal";

class ModalWrapper extends React.Component {
    render() {
        const {show, data, modalHandler} = this.props;
      return (
        <>
        {show && 
        <div className="modalWrapper" onClick = {(e) => {
            console.log(e.target.className)
            if(e.target.className === "modalWrapper" ) {
                modalHandler()
            }}}>
            <Modal data={data} modalHandler = {modalHandler} actions={
                <div className="buttons-wrapper">
                    <button className={`modal-button ${data.shadow ?"shadow" : ""}`} style={{marginRight:"10px"}}>{data.buttonOne}</button> 
            <button className={`modal-button ${data.shadow ?"shadow" : ""}`} onClick={()=>{
                modalHandler()
            }}>{data.buttonTwo} </button>
                </div>}></Modal>
        </div>
        }
        </>
      )
      }
}

export default ModalWrapper;  