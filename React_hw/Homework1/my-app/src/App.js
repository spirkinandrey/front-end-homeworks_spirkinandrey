import React from 'react';
import './App.scss';
import Button from "./components/Button/Button"
import ModalWrapper from './components/Modal/ModalWrapper';

class App extends React.Component {
  state = {
    showModal: false,
    modaldata: {},
  }
  ChangeModal = (newData) => {
    this.setState((state) => (
      { 
        showModal: true,
        modaldata: newData
      }
    ))
  }
  HideModal = () => {
    this.setState(
      { 
        showModal: false,
      }
    )
  }

  render () {
  return (
    <div className="App">
      <Button text={"Open First Modal"} background="red" dataHandler = {this.ChangeModal}></Button>
      <Button text={"Open Second Modal"} background="green"  dataHandler = {this.ChangeModal}></Button>
      <ModalWrapper show = {this.state.showModal} data = {this.state.modaldata} ModalHandler = {this.HideModal}>
    </ModalWrapper>
    </div>
  );
  }
}

export default App;
