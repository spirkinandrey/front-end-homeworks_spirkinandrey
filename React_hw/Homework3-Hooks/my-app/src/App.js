import React from 'react';
import './App.scss';
import ModalWrapper from './components/Modal/ModalWrapper';
import axios from "axios"
import CardsList from "./components/Cards List/CardsList"
import { Route } from "react-router-dom";
import Sidebar from "./components/Sidebar";
import PurchaseBin from "./components/Purchase Bin"
import Favorities from "./components/Favorities"



class App extends React.Component {
  state = {
    showModal: false,
    modaldata: {
            header: "",
            closeButton: true,
            text: "Andrey Bookshop is the best shop in the world?",
            background: "red",
            buttonOne: "Ok",
            buttonTwo: "Cancel",
            shadow: true,
    },
    currentGoodsData: {},
    goodscollection: [],
    favorities: [],
    purchaseBin: [],
    currentButtonText: "",
  }

  
  textHandler = (newtext) => {
    this.setState((state) =>(
      {
        currentButtonText: newtext
      }
    ))
  }

  modalHeaderHandler = (newText) => {
    const {modaldata} = this.state;
    if(newText === "add to cart") {
      modaldata.header = "Do you want to add book to your purchase bin?";
      this.setState((state) =>(
          {modaldata}
      ))
    } else if(newText === "Remove cart") {
      modaldata.header = "Do you want to remove book from your purchase bin?";
      this.setState((state) =>(
        {modaldata}
      ))
  }
  }
    
  favoritiesAdd = (favData) => {
    localStorage.removeItem('favorities')
    let a = 0;
    let favMas = this.state.favorities;
    favMas.forEach((el)=>{
      if(el === favData) {
        a++;
      }
    })
    if(a===0){
      favMas.push(favData)
      this.setState((state) =>(
        {
          favorities: favMas,
        }
      ))
    }
    localStorage.setItem('favorities', JSON.stringify(this.state.favorities))
  }
  favoritiesDelete = (favData) => {
    localStorage.removeItem('favorities')
    let favMas = this.state.favorities;
    favMas.forEach((item, index) =>{
      if(item === favData) {
        favMas.splice(index, 1)
        } 
      }
    )
    this.setState((state) =>(
      {
        favorities: favMas,
      }
    ))

    localStorage.setItem('favorities', JSON.stringify(this.state.favorities))
  }

  
  changeModal = (newData) => {
    this.setState((state) => (
      { 
        showModal: true,
        currentGoodsData: newData,
      }
    ))
  }

  hideModal = () => {
    this.setState(
      { 
        showModal: false,
      }
    )
  }

  addToPurchaseBin = () => {
    if(!localStorage.getItem(`${this.state.currentGoodsData.id}`)) {
      localStorage.setItem(`${this.state.currentGoodsData.id} - PurchaseBin`, JSON.stringify(this.state.currentGoodsData));
    }
    let purchaseMas = this.state.purchaseBin;
    purchaseMas.push(this.state.currentGoodsData)
    this.setState((state) =>(
      {
        purchaseBin: purchaseMas,
        showModal: false,
      }
    ))
  }



  removeFromPurchasebin = () => {
    let purchaseMas = this.state.purchaseBin;
    purchaseMas.forEach((item, index) =>{
      if(item === this.state.currentGoodsData) {
        purchaseMas.splice(index, 1)
        } 
      }
    )
    this.setState((state) =>(
      {
        purchaseBin: purchaseMas,
        showModal: false,
      }
    ))
  }

  componentDidMount () {
    axios('./goods.json').then(res => this.setState({goodscollection : res.data}));
  }

  render () {
  return (
    <div className="App">
      <Sidebar></Sidebar>
      <Route path="/CardsList" render={()=><CardsList data={this.state.goodscollection} dataHandler={this.changeModal} favoritiesDelete = {this.favoritiesDelete} favoritiesAdd = {this.favoritiesAdd} textHandler={this.textHandler} modalHeaderHandler={this.modalHeaderHandler}/>}></Route>
      <Route path="/Favorites" render={()=><Favorities data={this.state.favorities} dataHandler={this.changeModal} favoritiesDelete = {this.favoritiesDelete} favoritiesAdd = {this.favoritiesAdd} textHandler={this.textHandler} modalHeaderHandler={this.modalHeaderHandler}/>}></Route>
      <Route path="/Purchase Bin" render={()=><PurchaseBin purchaseItems={this.state.purchaseBin} textHandler={this.textHandler} dataHandler={this.changeModal} modalHeaderHandler={this.modalHeaderHandler} favoritiesDelete = {this.favoritiesDelete} favoritiesAdd = {this.favoritiesAdd}/>}></Route>
      {console.log(this.state.favorities)}
      <ModalWrapper text = {this.state.currentButtonText} show = {this.state.showModal} data = {this.state.modaldata} modalHandler = {this.hideModal} addToPurchaseBin = {this.addToPurchaseBin} removeFromPurchasebin= {this.removeFromPurchasebin}/>
    </div>
  );
  }
}

export default App;

