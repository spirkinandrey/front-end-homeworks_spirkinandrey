import React from "react"
import Card from "../Card/Card"


const PurchaseBin = ({purchaseItems, dataHandler, textHandler, modalHeaderHandler, favoritiesDelete, favoritiesAdd}) => {
    const text = "Remove cart";
    return (
        <div className="cardslist">
            {purchaseItems.map(e => (
                <Card text={text} key={e.id} data={e} dataHandler={dataHandler} favoritiesDelete = {favoritiesDelete} favoritiesAdd = {favoritiesAdd} textHandler={textHandler} modalHeaderHandler={modalHeaderHandler}></Card>
            ))}
        </div>
    )
}

export default PurchaseBin;