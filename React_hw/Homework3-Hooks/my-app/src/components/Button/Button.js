import React from 'react';
import PropTypes from "prop-types"

class Button extends React.Component {
    render() {
      const {text, background, dataHandler, textHandler, data, modalHeaderHandler} = this.props;

    return (
        <button className={`btn`} style={{backgroundColor: background}} onClick={() => {
          dataHandler(data)
          textHandler(text)
          modalHeaderHandler(text)
        }}>{text}</button> 
    )
    }
  }

  Button.propTypes ={
    text: PropTypes.string,
    background: PropTypes.string,
    dataHandler: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
  }

  Button.defaultProps = {
    text: "unknown button",
    background: "red",
  }

export default Button;  