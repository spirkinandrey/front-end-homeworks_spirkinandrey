import React from "react"
import Card from "../Card/Card"
import PropTypes from "prop-types"

class CardsList extends React.Component {
    render () {
        const { data, dataHandler, favoritiesDelete, favoritiesAdd, textHandler, modalHeaderHandler} = this.props;
        const text = "add to cart";
        return (
            <div className="cardslist">
                {data.map(e => (
                    <Card text={text} key={e.id} data={e} dataHandler={dataHandler} favoritiesDelete = {favoritiesDelete} favoritiesAdd = {favoritiesAdd} textHandler={textHandler} modalHeaderHandler={modalHeaderHandler}></Card>
                ))}
            </div>
        )
    }
}
CardsList.propTypes = {
    data: PropTypes.array,
    dataHandler: PropTypes.func.isRequired,
}
CardsList.defaultProps = {
    data: "You don`t have books in your shop"
}

export default CardsList;