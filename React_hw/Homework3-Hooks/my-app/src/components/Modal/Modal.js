import React from 'react'
import PropTypes from "prop-types"

class Modal extends React.Component {
    render() {
        const {data, actions, modalHandler} = this.props;
      return (
          <div className={`Modal`} style={{backgroundColor: data.background, color: data.color}}>
              <div className={`header-wrapper ${data.shadow ?"shadow" : ""}`}>
                <header>{data.header}</header>
                <>
                {data.closeButton && <span className="close" onClick={()=>{modalHandler()}}></span>}
                </>
              </div>
              <p className="main-text">{data.text}</p>
              {actions}
          </div>
      )
      }
}
Modal.propTypes = {
  data: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  modalHandler: PropTypes.func.isRequired,
}


export default Modal;  