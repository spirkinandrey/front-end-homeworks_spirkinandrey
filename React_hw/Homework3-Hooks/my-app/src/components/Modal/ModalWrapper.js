import React from 'react';
import Modal from "./Modal";
import PropTypes from "prop-types"


class ModalWrapper extends React.Component {
    render() {
        const {show, data, modalHandler, addToPurchaseBin, removeFromPurchasebin, text} = this.props;
      return (
        <>
        {show && 
        <div className="modalWrapper" onClick = {(e) => {
            console.log(e.target.className)
            if(e.target.className === "modalWrapper" ) {
                modalHandler()
            }}}>
            <Modal data={data} modalHandler = {modalHandler} actions={
                <div className="buttons-wrapper">
                    <button className={`modal-button ${data.shadow ?"shadow" : ""}`} style={{marginRight:"10px"}} onClick={()=>{
                        if(text === "add to cart") {
                            addToPurchaseBin();
                        } else if(text === "Remove cart") {
                            removeFromPurchasebin();
                        }
                    }}>{data.buttonOne}</button> 
                     <button className={`modal-button ${data.shadow ?"shadow" : ""}`} onClick={()=>{
                modalHandler()
            }}>{data.buttonTwo} </button>
                </div>}></Modal>
        </div>
        }
        </>
      )
      }
}
ModalWrapper.propTypes = {
    show: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired,
    modalHandler: PropTypes.func.isRequired,
    addToPurchaseBin: PropTypes.func.isRequired
}

export default ModalWrapper;  