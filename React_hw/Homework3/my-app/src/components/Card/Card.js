import React, {useState} from "react"
import "./card.scss";
import Button from "../Button/Button"
import PropTypes from "prop-types"

const Card = ({data, dataHandler, favoritiesDelete, favoritiesAdd, text, textHandler, modalHeaderHandler, activate}) => {
    const [active, setActive] = useState(false)
        const addToFavorities = (data) => {
            if(active||(activate===true)) {
                favoritiesDelete(data)
            } else if (!active&&(!activate))(
                favoritiesAdd(data)
            )
            setActive(!active);
        }
        
        return (
            <div className="card">
                <div className="card-picture">
                    <img src={data.picture} />
                </div>
                <div className="title-favor-wrapper">
                    <div className="card-title">{data.name}</div>
                    <div className="star">	  
                        <span className={active||activate ? "active" : ""} onClick={()=>{
                            addToFavorities(data)
                        }}></span>
                    </div>
                </div>
                <div className="price-id-wrapper">
                    <div className="price" style={{backgroundColor: (data.color === "black") ?  "white": data.color}}>{data.price} &euro;</div>
                    <div className="id"><Button text={text} background="black" data={data} dataHandler={dataHandler} textHandler={textHandler} modalHeaderHandler={modalHeaderHandler}></Button></div>
                </div>
            </div>
        )
}

Card.propTypes = {
    data: PropTypes.object,
    dataHandler: PropTypes.func.isRequired,
}
Card.defaultProps = {
    data: "no-book",
}

export default Card;