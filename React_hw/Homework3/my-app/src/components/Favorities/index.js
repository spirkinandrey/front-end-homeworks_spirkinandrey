import React from"react"
import Card from "../Card/Card"

const Favorities = ({data, dataHandler, textHandler, modalHeaderHandler, favoritiesDelete, favoritiesAdd}) => {
    const activate = true;
    const text = "add to cart";
    return (
        <div className="cardslist">
            {data.map(e => (
                <Card text={text} key={e.id} data={e} dataHandler={dataHandler} favoritiesDelete = {favoritiesDelete} favoritiesAdd = {favoritiesAdd} textHandler={textHandler} modalHeaderHandler={modalHeaderHandler} activate={activate}></Card>
            ))}
        </div>
    )
}

export default Favorities;