import React from "react";
import { Link } from "react-router-dom";

const Sidebar = () => {
    return (
        <div className="sidebar">
            <Link className="link" to="/CardsList">CardsList</Link>
            <Link className="link" to="/Favorites">Favorites</Link>
            <Link className="link" to="Purchase Bin">Purchase Bin</Link>
        </div>
    )
}
 export default Sidebar;