import React, { useState, useEffect } from 'react';
import './App.scss';
import ModalWrapper from './components/Modal/ModalWrapper';
import axios from "axios"
import CardsList from "./components/Cards List/CardsList"
import { Route } from "react-router-dom";
import Sidebar from "./components/Sidebar";
import PurchaseBin from "./components/Purchase Bin"
import Favorities from "./components/Favorities"



const App = () => {
  const [showModal, setshowModal] = useState(false);
  const [modaldata, setmodalData] = useState({
    header: "",
    closeButton: true,
    text: "Andrey Bookshop is the best shop in the world?",
    background: "red",
    buttonOne: "Ok",
    buttonTwo: "Cancel",
    shadow: true,
  });
  const [currentGoodsData, setcurrentGoodsData] = useState([]);
  const [goodscollection, setgoodscollection] = useState([]);
  const [favorities, setfavorities] = useState([]);
  const [purchaseBin, setpurchaseBin] = useState([]);
  const [currentButtonText, setcurrentButtonText] = useState("");
  // state = {
  //   showModal: false,
  //   modaldata: {
  //           header: "",
  //           closeButton: true,
  //           text: "Andrey Bookshop is the best shop in the world?",
  //           background: "red",
  //           buttonOne: "Ok",
  //           buttonTwo: "Cancel",
  //           shadow: true,
  //   },
  //   currentGoodsData: {},
  //   goodscollection: [],
  //   favorities: [],
  //   purchaseBin: [],
  //   currentButtonText: "",
  // }

  
  const textHandler = (newtext) => {
    setcurrentButtonText(newtext);
  }

  const modalHeaderHandler = (newText) => {
    if(newText === "add to cart") {
      setmodalData.header = "Do you want to add book to your purchase bin?";

    } else if(newText === "Remove cart") {
      setmodalData.header = "Do you want to remove book from your purchase bin?";
    }
  }
    
  const favoritiesAdd = (favData) => {
    localStorage.removeItem('favorities')
    let a = 0;
    let favMas = favorities;
    favMas.forEach((el)=>{
      if(el === favData) {
        a++;
      }
    })
    if(a===0){
      favMas.push(favData)
      setfavorities(favMas);
    }
    localStorage.setItem('favorities', JSON.stringify(favorities))
  }
  const favoritiesDelete = (favData) => {
    localStorage.removeItem('favorities')
    let favMas = favorities;
    favMas.forEach((item, index) =>{
      if(item === favData) {
        favMas.splice(index, 1)
        } 
      }
    )
    setfavorities(favMas);
    localStorage.setItem('favorities', JSON.stringify(favorities))
  }

  
  const changeModal = (newData) => {
    setshowModal(true);
    setcurrentGoodsData(newData);
  }

  const hideModal = () => {
    setshowModal(false);
  }

  const addToPurchaseBin = () => {
    if(!localStorage.getItem(`${currentGoodsData.id}`)) {
      localStorage.setItem(`${currentGoodsData.id} - PurchaseBin`, JSON.stringify(currentGoodsData));
    }
    let purchaseMas = purchaseBin;
    purchaseMas.push(currentGoodsData)
    setpurchaseBin(purchaseMas);
    setshowModal(false);
  }



  const removeFromPurchasebin = () => {
    let purchaseMas = purchaseBin;
    purchaseMas.forEach((item, index) =>{
      if(item === currentGoodsData) {
        purchaseMas.splice(index, 1)
        } 
      }
    )
    setpurchaseBin(purchaseMas);
    setshowModal(false);
  }

  useEffect (() =>{
    axios('./goods.json').then(res => setgoodscollection(res.data));
  });

  return (
    <div className="App">
      <Sidebar></Sidebar>
      <Route path="/CardsList" render={()=><CardsList data={goodscollection} dataHandler={changeModal} favoritiesDelete = {favoritiesDelete} favoritiesAdd = {favoritiesAdd} textHandler={textHandler} modalHeaderHandler={modalHeaderHandler}/>}></Route>
      <Route path="/Favorites" render={()=><Favorities data={favorities} dataHandler={changeModal} favoritiesDelete = {favoritiesDelete} favoritiesAdd = {favoritiesAdd} textHandler={textHandler} modalHeaderHandler={modalHeaderHandler}/>}></Route>
      <Route path="/Purchase Bin" render={()=><PurchaseBin purchaseItems={purchaseBin} textHandler={textHandler} dataHandler={changeModal} modalHeaderHandler={modalHeaderHandler} favoritiesDelete = {favoritiesDelete} favoritiesAdd = {favoritiesAdd}/>}></Route>
      {console.log(favorities)}
      <ModalWrapper text = {currentButtonText} show = {showModal} data = {modaldata} modalHandler = {hideModal} addToPurchaseBin = {addToPurchaseBin} removeFromPurchasebin= {removeFromPurchasebin}/>
    </div>
  );
}

export default App;

