import React from 'react';
import './App.scss';
import ModalWrapper from './components/Modal/ModalWrapper';
import axios from "axios"
import CardsList from "./components/Cards List/CardsList"


class App extends React.Component {
  state = {
    showModal: false,
    modaldata: {
            header: "Do you want to add book to your purchase bin?",
            closeButton: true,
            text: "Andrey Bookshop is the best shop in the world?",
            background: "red",
            buttonOne: "Ok",
            buttonTwo: "Cancel",
            shadow: true,
    },
    currentGoodsData: {},
    goodscollection: [],
    favorities: [],
  }
  favoritiesAdd = (favData) => {
    localStorage.removeItem('favorities')
    let favMas = this.state.favorities;
    favMas.push(favData)
    this.setState((state) =>(
      {
        favorities: favMas,
      }
    ))

    localStorage.setItem('favorities', JSON.stringify(this.state.favorities))
  }
  favoritiesDelete = (favData) => {
    localStorage.removeItem('favorities')
    let favMas = this.state.favorities;
    favMas.forEach((item, index) =>{
      if(item === favData) {
        favMas.splice(index, 1)
        } 
      }
    )
    this.setState((state) =>(
      {
        favorities: favMas,
      }
    ))

    localStorage.setItem('favorities', JSON.stringify(this.state.favorities))
  }

  
  changeModal = (newData) => {
    this.setState((state) => (
      { 
        showModal: true,
        currentGoodsData: newData,
      }
    ))
  }
  hideModal = () => {
    this.setState(
      { 
        showModal: false,
      }
    )
  }

  addToPurchaseBin = () => {
    if(!localStorage.getItem(`${this.state.currentGoodsData.id}`)) {
      localStorage.setItem(`${this.state.currentGoodsData.id} - PurchaseBin`, JSON.stringify(this.state.currentGoodsData));
    }
    this.setState(
      { 
        showModal: false,
      }
    )
  }

  componentDidMount () {
    axios('./goods.json').then(res => this.setState({goodscollection : res.data}));
  }

  render () {
  return (
    <div className="App">
      <CardsList data={this.state.goodscollection} dataHandler={this.changeModal} favoritiesDelete = {this.favoritiesDelete} favoritiesAdd = {this.favoritiesAdd}/>
      <ModalWrapper show = {this.state.showModal} data = {this.state.modaldata} modalHandler = {this.hideModal} addToPurchaseBin = {this.addToPurchaseBin}/>
    </div>
  );
  }
}

export default App;

