let themeChanger = document.createElement("p")
themeChanger.className = "theme-box";
themeChanger.innerHTML = "Change theme";
document.body.prepend(themeChanger);
let link = document.querySelectorAll("link");

let rightLink;
link.forEach((item)=> {
    if (item.getAttribute("href") ==="CSS/style.css"||item.getAttribute("href") ==="CSS/style-dark.css"){
        return rightLink = item;
    }
});
if (localStorage.getItem("dark_theme") === null) {
    rightLink.setAttribute("href", "CSS/style.css");
} else {
    rightLink.setAttribute("href", localStorage.getItem("dark_theme"));
}

console.log(localStorage.getItem("dark_theme"));
themeChanger.addEventListener('click', ()=>{
    let darkLink;
    if (rightLink.getAttribute("href") === "CSS/style.css") {
        rightLink.removeAttribute("href");
        localStorage.setItem("dark_theme", "CSS/style-dark.css");
        darkLink = localStorage.getItem("dark_theme");
        rightLink.setAttribute("href", darkLink);
    } else if (rightLink.getAttribute("href") === "CSS/style-dark.css") {
        rightLink.removeAttribute("href");
        rightLink.setAttribute("href", "CSS/style.css");
        localStorage.removeItem("dark_theme");
    }
});


