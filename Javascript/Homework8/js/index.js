let input = document.querySelector('#price_input');
input.style.border = "1px solid black";
input.addEventListener('focus', () => {
        input.style.border = "1px solid green";
        input.style.outlineColor = "green";
    }
);

input.addEventListener('change',() => {
        let priceLabel = document.querySelector('.price-label');
        if (priceLabel !== null&&Number(input.value >=0)) {
            let priceValue = document.querySelector('#priceValue');
            priceValue.innerText = `Current price ${input.value}`; }
            else if (priceLabel !== null&&Number(input.value < 0)) {
                priceLabel.remove();
            input.style.border = "1px solid red";
            input.style.outlineColor = "red";
            let spanError = document.createElement('span');
            spanError.className = 'price-error';
            spanError.innerHTML = 'Input correct price please';
            input.after(spanError);
            } else if (Number(input.value < 0)) {
            input.style.border = "1px solid red";
            input.style.outlineColor = "red";
            let spanError = document.createElement('span');
            spanError.className = 'price-error';
            spanError.innerHTML = 'Input correct price please';
            input.after(spanError);
            } else {
                let priceLabel = document.createElement("div");
                priceLabel.className = "price-label";
                input.before(priceLabel);
                let priceValue = document.createElement("span");
                priceValue.id ="priceValue";
                priceLabel.prepend(priceValue);
                let closeIcon = document.createElement("i");
                closeIcon.id = "clear_price";
                closeIcon.className = "fas fa-times-circle";
                priceLabel.append(closeIcon);
                priceValue.innerText = `Current price ${input.value}`;
                priceLabel.classList.add('price-label');
                input.style.color = "green";
            let clearPrice = document.querySelector('#clear_price');
            clearPrice.addEventListener('click', () => {
                let priceLabel = document.querySelector('.price-label');
                priceLabel.remove();
                input.value = null;
                input.style.color = "black";
                input.style.border = "1px solid black";
            });
            let spanError = document.querySelector('.price-error');
            spanError.remove();
    }
});




