let wrapper = document.querySelector(".images-wrapper");
let imgs = document.querySelectorAll(".image-to-show");
time = 10000;
let stop = document.createElement("div");
stop.innerHTML = "stop";
stop.className = "stop-button";
wrapper.after(stop);
stop.addEventListener("click", ()=>{
        clearInterval(intervalSetter);
    }
);

let proceed = document.createElement("div");
proceed.innerHTML = "proceed";
proceed.className = "proceed-button";
stop.after(proceed);
proceed.addEventListener("click", ()=>{
        setInterval (showPictures, time);
    }
);
let secWrap = document.createElement("span");
secWrap.className = "sec-wrap";
proceed.after(secWrap);
let miliSecWrap = document.createElement("span");
miliSecWrap.className = "mili-sec-wrap";
secWrap.after(miliSecWrap);

let timerSec = function() {
    let timeOneSec = 1000;
    setInterval (handlerSec, timeOneSec);
};
let sec = 0;
handlerSec = () => {
    if (sec < 10) {
        sec = sec + 1;
    }
    return secWrap.innerHTML = `Sec ${sec}`;
};
let timerMiliSec = function() {
    let timeOneMiliSec = 1;
    setInterval (handlerMiliSec, timeOneMiliSec);
};
let milicec = 0;
handlerMiliSec = () => {
    if (milicec < 1000) {
        milicec = milicec + 1;
    }
    return miliSecWrap.innerHTML = `Sec ${milicec}`;
};
function showPictures () {
    let index = null;
    imgs.forEach((item, i) => {
        if (item.classList.contains("visible")){
            index = i;
        }
        return index;
    });
    if (index < imgs.length - 1) {
        imgs[index].classList.remove("visible");
        imgs[index+1].classList.add("visible");
        console.log(imgs[index]);
    } else {
        imgs[imgs.length-1].classList.remove("visible");
        imgs[0].classList.add("visible");
    }

    timerSec();
    timerMiliSec();
}



let intervalSetter = setInterval (showPictures, time);




/*
let secWrap = document.createElement("span");
secWrap.className = "sec-wrap";
proceed.after(secWrap);
let miliSecWrap = document.createElement("span");
miliSecWrap.className = "mili-sec-wrap";
secWrap.after(miliSecWrap);

let timerSec = function() {
    let timeOneSec = 1000;
    setInterval (handlerSec, timeOneMSec);
};
handlerSec = () => {
    let sec = 0;
    if (sec < 10) {
        sec = sec + 1;
        }
    return secWrap.innerHTML = `Sec ${sec}`;
    };
let timerMiliSec = function() {
    let timeOneMiliSec = 1;
    setInterval (handlerMiliSec, timeOneMiliSec);
};

handlerMiliSec = () => {
    let milicec = 0;
    if (milicec < 1000) {
        milicec = milicec + 1;
    }
    return miliSecWrap.innerHTML = `Sec ${milicec}`;
};*/