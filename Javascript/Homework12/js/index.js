let wrapper = document.querySelector(".images-wrapper");
let imgs = document.querySelectorAll(".image-to-show");

time = 10000;


let stop = document.createElement("div");
stop.innerHTML = "stop";
stop.className = "stop-button";
wrapper.after(stop);

let proceed = document.createElement("div");
proceed.innerHTML = "proceed";
proceed.className = "proceed-button";
stop.after(proceed);


let i = 0;
function showPictures () {
    if(i<imgs.length-1) {
        i = i + 1;
        let pictureAdress = imgs[i].getAttribute("src");
        imgs[0].setAttribute("src", `${pictureAdress}`);
    } else if (i === imgs.length- 1) {
        i = 0;
        imgs[0].setAttribute("src", `./img/img_1.jpg`);
    }
}

let intervalSetter = setInterval (showPictures, time);

stop.addEventListener("click", () => {
    clearInterval(intervalSetter);
});

proceed.addEventListener("click", () => {
    clearInterval(intervalSetter);
    intervalSetter = setInterval (showPictures, time);
});

