
// Code shows information about service after click on service in menu;
let serviceWrapper = document.querySelector(".services-wrapper");
let service = document.querySelectorAll(".service");
let serviceInfo = document.querySelectorAll(".service-info");

serviceWrapper.addEventListener("click", (e)=> {
    let target = e.target;
    if(target.classList.contains("active")) {
    } else {
        service.forEach( (item)=> {
            if(item.classList.contains("active")) {
                item.classList.remove("active");
                target.classList.add("active");
            }
        })
    }
    serviceInfo.forEach((element)=>{
        if((target.innerText === element.id)&&(element.getAttribute("hidden") === "hidden")) {
            element.removeAttribute("hidden");
        } else if ((target.innerText !== element.id)&&(element.getAttribute("hidden") === null)){
            element.setAttribute("hidden", "hidden");
        }
    })
});
// Download of additional images and choose them
let loadMore = document.querySelector(".load-more");
let works = document.querySelectorAll(".work-pic");
loadMore.addEventListener("click", ()=>{
    let animation = document.createElement("div");
    animation.className = "loading";
    loadMore.before(animation);
    setTimeout(()=>{
        animation.remove();
        loadMore.remove();
        works.forEach((element)=>{
            loadMore.setAttribute("disabled", "disabled");
            if(element.classList.contains("hidden") === true) {
                element.classList.remove("hidden");
                element.classList.remove("download");
            }
        });
    }, 2000);

});


let  webDesignWrapper = document.querySelectorAll(".web-design-wrapper");
let workPicsWrapper = document.querySelector(".works-links-wrapper");
workPicsWrapper.addEventListener("click", (e)=> {
    let target = e.target;
    console.log(target.id)
    webDesignWrapper.forEach( (element) =>{
        if((target.innerText === "All")&&(element.classList.contains("download") === false)) {
            element.classList.remove("hidden")
        }   else if((element.classList.contains(target.id) === true)&&(element.classList.contains("download") === false)){
            element.classList.remove("hidden");
        } else {
            element.classList.add("hidden");
        }
    })
});

