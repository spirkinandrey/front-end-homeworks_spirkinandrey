let button = document.querySelector(".head-nav__button");
let topLine = button.firstElementChild;
let secondLine = topLine.nextElementSibling;
let thirdLine = button.lastElementChild;
let navMenu = document.querySelector(".head-nav__menu");
let navLinks = document.querySelectorAll(".head-nav__menu__link");

button.addEventListener("click", ()=> {
    if (topLine.classList.contains("active") === true) {
        topLine.classList.remove("active");
        secondLine.classList.remove("active");
        thirdLine.classList.remove("active");
        navMenu.classList.remove("active");
        navMenu.classList.add("container");
        navMenu.classList.add("hidden");
        navLinks.forEach((element) => {
            if (element.classList.contains("active") === true) {
                element.classList.remove("active");
            }
        })
    } else {
        topLine.classList.add("active");
        secondLine.classList.add("active");
        thirdLine.classList.add("active");
        navMenu.classList.add("active");
        navMenu.classList.remove("container");
        navMenu.classList.remove("hidden");
        navLinks.forEach((element) => {
            if (element.classList.contains("active") === false) {
                element.classList.add("active");
            }
        })
    }
});