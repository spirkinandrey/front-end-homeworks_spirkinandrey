const gulp = require('gulp');

module.exports = function fonts() {
    return gulp.src('src/fonts/**/*.ttf').pipe(gulp.dest("dist/fonts"));
};