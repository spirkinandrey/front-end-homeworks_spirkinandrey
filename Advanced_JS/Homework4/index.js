let root = document.querySelector(".root");

async function getFilms(url) {
    let response = await fetch(url).then((response) =>{
        return response.json();
    }).then((data) => data);
    return response;
}
async function getCharacters (data) {
    data.forEach((item) => {
        let filmId = document.getElementById(`${item.episode_id}`);
        let charactersList = item.characters;
        charactersList.forEach(async (item) => {
            await fetch(item).then((response)=> response.json()).then((data)=> {
                filmId.insertAdjacentHTML("beforeend", `<p>${data.name}</p>`);
                console.log(data)
            });
        });
    });
}
async function processFilmList(url) {
    let films = await getFilms(url);
    let filmList = films.results;
    console.log(filmList);

    filmList.forEach((item) => {
        let id = item.episode_id;
        root.insertAdjacentHTML("beforeend", `<div class="film-item" id=${id}> 
                                                                 <p>${item.title}</p>
                                                                 <p>${item.episode_id}</p>
                                                                 <p>${item.opening_crawl}</p>
                                                            </div>`)
    });
    await getCharacters(filmList);
}


processFilmList("https://swapi.dev/api/films");