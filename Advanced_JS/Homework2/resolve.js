const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
let root = document.querySelector("#root");
let ul =  document.createElement('ul');
let li =  document.createElement('li');

function UserException(message) {
    this.message = message;
    this.name = "UserException";
}
function listCreator (dataArray) {
    let errorItems = [];
    let ul = document.createElement('ul');
    let arrayTransformedToList = dataArray.map((item) => {
        if(item.name !== undefined && item.author !== undefined && item.price !== undefined) {
            let li = document.createElement('li');
            let content = "";
             for (let i in item) {
                 content += i + " " + item[i] + " ";
                }
             li.textContent = content;
             return li;
        } else {
            errorItems.push(item);
        }
    });
    arrayTransformedToList.forEach((el) => {
        if(el !== undefined) {
            ul.append(el);
        }
    });
    function findError (item) {
        if (item.name === undefined) {

            throw new UserException(`field Name is empty`);
        } else if (item.author === undefined) {
            throw new UserException(`field Author is empty`)
        } else if (item.price === undefined) {
            throw new UserException(`field Price is empty`);
        }
    }
    errorItems.forEach((item) => {
        try {
            console.log(item);
            findError(item);
        }
        catch (e) {
            console.log(e);
        }
    });

    return root.append(ul);
}

listCreator(books);




