class Employee {
    constructor(name = "Ernst", age = "18", salary = 1000) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get emplName() {
        return this.name;
    }
    get emplAge() {
        return this.age;
    }
    get emplSalary() {
        return this.salary;
    }
    set emplName(value) {
        this.name = value;
    }
    set emplAge(value) {
        this.age = value;
    }
    set emplSalary(value) {
        this.salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get progSalary() {
        return this.salary*3;
    }
    get progLang() {
        return this.lang;
    }
    set progLang(value) {
        this.lang = value;
    }
}

let andrew = new Programmer("Andrey", 35, 1000, 2,);
let alex = new Programmer("Alexey", 34, 10000, 0);
let vitya = new Programmer("Victor", 34, 2000, 5);

console.log(andrew, andrew.progSalary, alex, alex.progSalary, vitya, vitya.progSalary);