let ipButton = document.querySelector(".ip__search");
let mainField = document.querySelector(".main-field");

const info = async function searchIp (url) {
    const response = await fetch(url).then((response) => {
        return response.json();
    })
        .then((data) => {
            return data.ip;
        });
    return response;
};
function placeRequest(data) {
    let infoItems = document.querySelectorAll(".info-item");
    if (infoItems.length !== 0) {
        for (let i = 0; i < infoItems.length; i++) {
            infoItems[i].remove();
        }
    }
    fetch(`http://ip-api.com/json/${data}`)
        .then((response) => {
            return response.json();
        }).then((data) => {
        console.log(data);
        console.log(data.city);
        let content = "";
        for (let i in data) {
            content = i + " " + data[i] + " ";
            mainField.insertAdjacentHTML("beforeend", `<div class="info-item"> ${content}</div>`)
        }
    });
}
ipButton.addEventListener(("click"), async()=> {
    let ip = await info("https://api.ipify.org/?format=json");
    placeRequest(ip);
    console.log(ip);
});