//Task 1. Merged base finally in clients1 base
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
console.log("Task1 result");
let merged = [...new Set([...clients1,...clients2])];
console.log(merged);

//Task 2.

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vampire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

let charactersShortInfo = characters.map(function (item) {
    let personInfo = {};
    personInfo.name = item.name;
    personInfo.lastName = item.lastName;
    personInfo.age = item.age;
    return personInfo;
})
console.log("Task2 result");
console.log(charactersShortInfo);

// Task 3.

const user1 = {
    name: "John",
    years: 30
};
const {name, years: age, isAdmin: isAdmin = false } = user1;
console.log("Task3 result");
console.log(name, age, isAdmin);