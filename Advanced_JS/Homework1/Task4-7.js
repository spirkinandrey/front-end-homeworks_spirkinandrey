//Task 4.
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
};

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
};

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
};
console.log("Task4 result");
const fullBio = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log(fullBio);

//Task 5.
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
};

const newBook = {bookToAdd, ...books};
console.log("Task5 result");
console.log(newBook);

//Task 6.
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
};
console.log("Task6 result");
const employeeImproved = {...employee, age: 45, salary: 50000};
console.log(employeeImproved);

//Task 7.
const array = ['value', () => 'showValue'];
const [value, showValue] = array;

alert(value); // должно быть выведено 'value'
alert(showValue());  // должно быть выведено 'showValue'
