import React from 'react';

class Modal extends React.Component {
    render() {
        const {data, actions, modalHandler} = this.props;
      return (
          <div className={`Modal`} style={{backgroundColor: data.background, color: data.color}}>
              <div className={`header-wrapper ${data.shadow ?"shadow" : ""}`}>
                <header>{data.header}</header>
                <>
                {data.closeButton && <span className="close" onClick={()=>{modalHandler()}}></span>}
                </>
              </div>
              <p className="main-text">{data.text}</p>
              {actions}
          </div>
      )
      }
}

export default Modal;  