import React from 'react';
import './App.scss';
import Button from "./components/Button/Button"
import ModalWrapper from './components/Modal/ModalWrapper';

class App extends React.Component {
  state = {
    showModal: false,
    modaldata: {},
  }
  changeModal = (newData) => {
    this.setState((state) => (
      { 
        showModal: true,
        modaldata: newData
      }
    ))
  }
  hideModal = () => {
    this.setState(
      { 
        showModal: false,
      }
    )
  }

  render () {
  return (
    <div className="App">
      <Button text={"Open First Modal"} background="red" dataHandler = {this.changeModal} / >
      <Button text={"Open Second Modal"} background="green"  dataHandler = {this.changeModal} />
      <ModalWrapper show = {this.state.showModal} data = {this.state.modaldata} modalHandler = {this.hideModal}>
    </ModalWrapper>
    </div>
  );
  }
}

export default App;
