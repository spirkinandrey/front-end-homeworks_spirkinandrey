import React from "react"
import Card from "../Card/Card"
import PropTypes from "prop-types"

class CardsList extends React.Component {
    render () {
        const { data , dataHandler } = this.props;
        return (
            <div className="cardslist">
                {data.map(e => (
                    <Card key={e.id} data={e} dataHandler={dataHandler} ></Card>
                ))}
            </div>
        )
    }
}
CardsList.propTypes = {
    data: PropTypes.array,
    dataHandler: PropTypes.func.isRequired,
}
CardsList.defaultProps = {
    data: "You don`t have books in your shop"
}

export default CardsList;