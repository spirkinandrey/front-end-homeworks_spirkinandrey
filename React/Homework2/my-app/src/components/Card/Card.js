import React from "react"
import "./card.scss";
import Button from "../Button/Button"
import PropTypes from "prop-types"

class Card extends React.Component {
    state = {
        active: false,
    }
    render() {
        const addToFavorities = () =>{
            if(this.state.active) {
                localStorage.removeItem(`${data.id} favorities`);
            } else {
                localStorage.setItem(`${data.id} favorities`, JSON.stringify(data))
            }
            this.setState((state) => (
                { 
                    active: !this.state.active,
                }
            ))
        }

        const {data, dataHandler} = this.props;
        return (
            <div  className="card">
                <div className="card-picture">
                    <img src={data.picture} />
                </div>
                <div className="title-favor-wrapper">
                    <div className="card-title">{data.name}</div>
                    <div className="star">	  
                        <span className={this.state.active ? "active" : ""} onClick={()=>{
                            addToFavorities()
                        }}></span>
                    </div>
                </div>
                <div className="price-id-wrapper">
                    <div className="price" style={{backgroundColor: (data.color === "black") ?  "white": data.color}}>{data.price} &euro;</div>
                    <div className="id"><Button text={"Add to cart"} background="black" data={data} dataHandler={dataHandler}></Button></div>
                </div>
            </div>
        )
    }
}
Card.propTypes = {
    data: PropTypes.object,
    dataHandler: PropTypes.func.isRequired,
}
Card.defaultProps = {
    data: "no-book",
}

export default Card;