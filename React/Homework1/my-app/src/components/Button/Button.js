import React from 'react';

class Button extends React.Component {
    

    render() {
      const {text, background, dataHandler} = this.props;
      const chooseData = (text) => {
        if(text === "Open First Modal") {
          return {
            header: "Do you want to delete this?",
            closeButton: true,
            text: "Once wou delete this file, It won`t be possible to undo this action. Are you sure you want to delete  it?",
            background: "red",
            buttonOne: "Ok",
            buttonTwo: "Cancel",
            shadow: true,
          }
        } else if (text === "Open Second Modal") {
           return {
            header: "Do you want to create a new project?",
            closeButton: true,
            text: "You can easily handle a new projects and have access to your work from any device",
            background: "green",
            buttonOne: "Create project",
            buttonTwo: "A am already have a project",
            color: "black",
          }
        }
      } 
    return (
        <button className={`btn`} style={{backgroundColor: background}} onClick={() => {dataHandler(chooseData(text))}}>{text}</button> 
    )
    }
  }

export default Button;  